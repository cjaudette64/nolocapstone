INSTRUCTIONS AND SETUP FOR NOLO WIZARD


-----------------------------------------------------------------
Specifications:
 - Windows 10 or better
 - Microsoft Excel
 - Microsoft Visual Studio (if code modification is necessary)


-----------------------------------------------------------------
Product Installation
1. Download and save the git repository containing the full project.
2. Extract all files onto the desktop or desired location.
3. Open the file folder.
4. Navigate to the sub directory NoLo > NoLoWizardSetup > Debug
5. Double click the "setup.exe" file 
6. Follow the prompts provided by the setup wizard.
7. Click finish once you reach the end of the wizard.
	NOTE: The application should now be located on the users desktop
8. Double click the application to open it.


-----------------------------------------------------------------
Product Usage
1. Click the "Bookstore CSV File".
2. Select the file that is designed for the bookstore CSV.
3. Verify the infomration has updated on the application and on the console window in the background.
4. Click the "Room CSV File" if one is needed. Otherwise, skip to step 7.
5. Select the file that is designed for the room CSV.
6. Verify the infomration has updated on the application and on the console window in the background.
7. Use the numeric entry window to specify the NoLo Threshold.
8. Click the "Calculate" button.
9. Save the CSV file to the desired location once the window pops up.
10. Wait 10 seconds for the file to fully compile and finish computing the NoLo information.


-----------------------------------------------------------------
Program Specifications using Visual Studio (Running command line only)
1. Open the project in Microsoft Visual Studio.
2. Right click the project level.
3. Select the properties list item or use "Alt+Enter".
4. Navigate to Configuration Properties > Linker > System.
5. Change the "SubSystem" attribute to Console (/SUBSYSTEM:CONSOLE). 
	NOTE: If this is not changed, the output for the command line will not work properly. 
	This will also only function for the bookstore CSV as requested.
6. Navigate to Configuration Properties > Debugging.
7. Change the "Command Arguments" to specify the file to be tested. This is only if you are using a single file entry instead of the command line.


-----------------------------------------------------------------
Helpful Tips
 - If you are struggling with file outputs, start by verifying that the files are indicated as CSV files. If they are indicated as a .xlsx file, 
	open the file in Microsoft Excel, click File > Export > Change File Type > CSV (comma delimited) (*csv) > Save As.
 - If you need to clear the application and create a new NoLo output file, select the "Reset" button at the bottom of the screen. All of 
	the fields and files will be cleared from the application. Then follow the product usage section to create a new NoLo output file.
 - If the application window is not closing properly, use the "Exit" button at the bottom of the screen to close the application.
 - If further help is required, there is a "Help" button located in the upper right hand corner of the screen for more information and help.