#pragma once

namespace NoLo {
    
    const int EXIT_ERROR_NO_ARGS = 1;
    const int EXIT_ERROR_FILE_NOT_FOUND = 2;
    const int EXIT_FILE_INVALID = 3;

    const char* const TESTDATA_DIR = "TestFile5Multi.csv";

    int CommandLine(cli::array<String^> ^argc);

}