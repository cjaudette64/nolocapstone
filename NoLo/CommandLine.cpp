/*
File: CommandLine.cpp

Course: CSCI285N
Lab Assignment: Capstone NoLo Project

Description: Implementation of file read in and output for bookstore and room CSV information.
This file is the main frame of the coding project and processses for running the command line.
The functions within this file will evaluate each CSV file for the bookstore and the room csv 
and capture the required information necessary.

Author: Cameron Audette
Date: 04/22/2021

*/

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <iterator>
#include <regex>
#include <map>
#include <string_view>

#include "NoLoForm.h"
#include "Course.h"
#include "utils.h"
#include "CommandLine.h"

using namespace NoLo;

int NoLo::CommandLine(cli::array<String^> ^argc) {

    // This retrieves the NoLo threshold provided by the user
    double noloValue = 40;


    // These constants are used to pull
    // Specific columns from the bookstore csv
    const int SECTION = 0;
    const int CHECKER = 1;
    const int REQ = 2;
    const int AUTHOR = 4;
    const int TITLE = 5;
    const int DURATION = 10;
    const int ISBN = 11;
    const int PRICE = 13;

    using namespace Runtime::InteropServices;
    const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(argc[0])).ToPointer();
    std::string str = chars;

    std::ifstream infile(str);                      // Reads the bookstore csv file into the program
    std::map<std::string, Course> courses;          // Creates a map and key pair for the courses class
    std::string line;                               // String to hold each line within the CSV file
    bool fileIntegrity = false;                     // Indicates whether the file is valid or not

    //This if statement ensures that the file is running properly.
    if (infile.good()) {
        Course currCourse;                              // Creates a new course object called currCourse

        // While the file can be read in and a line can be retrieved from it,
        // Start the parsing process
        while (getline(infile, line)) {

            // Creates a vector of strings that contains each line of the CSV file.
            // Each line is separated into commas to better read the file
            std::vector<std::string> bookVector = split(line, ',');

            // This file checks to verify that the file is valid
            if (bookVector[CHECKER] == "COURSE HISTORY REPORT") {
                fileIntegrity = true;
            }
            // OBTAINING THE COURSE SECTION INFORMATION
            // If the current line being analyzed is not empty and the line doesnt start with
            // the title course, obtain the course information
            if (fileIntegrity) {
                if (bookVector[SECTION].length() != 0 && bookVector[SECTION] != "COURSE") {
                    if (currCourse.getID() != "") {
                        courses[currCourse.getID()] = currCourse;       // Saves the course section inforation
                                                                        // In the new class
                    }
                    currCourse = Course(bookVector);                    // Saves the current course information
                                                                        // In currCourse to be used to get the 
                                                                        // Courses materials
                }


                // OBTAINING A COURSES MATERIALS
                // If the line being read in is a material, this section is performed.
                // If the current course is not empty, we begin to look
                // At the materials for that course
                else {
                    if (currCourse.getID() != "") {

                        // If the material is required and is found in the line, keep going
                        if (bookVector[REQ].find("REQ") != std::string::npos) {

                            // If the line contains PURCHASE or N/A, keep looking into the material
                            if (bookVector[DURATION].find("PURCHASE") != std::string::npos || bookVector[DURATION].find("N/A") != std::string::npos) {

                                // If the material has an ISBN, the material is a book and is added to
                                // The material vector and is assigned to the current course
                                if (bookVector[ISBN] != "") {
                                    Material currMaterial(bookVector);
                                    currCourse.addMaterial(currMaterial);
                                }
                            }
                        }
                    }
                }
            }
        }



        // This ensures that all of the course IDs have been saved
        // And placed in the courses class.
        if (currCourse.getID() != "") {
            courses[currCourse.getID()] = currCourse;
        }
    }

    infile.close();

    // This function uses the previous if statement to identify
    // If the file included was valid. If not, the file will
    // Output an error and exit the running process.
    if (fileIntegrity == false) {
        cout << "FILE INVALID FOR: BOOKSTORE CSV" << endl;
        return EXIT_FILE_INVALID;
    }

    cout << "CRN,SUBJECT,NUMBER,SECTION,COURSE NAME,COST,NOLO,INSTRUCTOR NAME,INSTRUCTOR EMAIL" << endl;

    // This for loop checks each element within the courses vector map
    // and first looks to see if the room file was give.
    for (std::pair<std::string, Course> element : courses) {

        // This function calculates the NoLo factor for the output.
        // If the calculated total cost is less than the NoLo
        // Threshold provided, then the course is declared as NoLo
        double savedCost = element.second.totalCost();          // Holds the courses overall cost
        std::string noloResult = "";                            // Contains if the course is NoLo
        if (savedCost <= noloValue) {
            noloResult = "NOLO";
        }

        // This output statement prints out all of the courses information to the csv file
        // under the name "NoloOutput.csv"
        cout << element.second.getCRN() << "," << element.second.getSubject() << "," << element.second.getNumber() << "," << element.second.getSection()
            << "," << element.second.getName() << "," << element.second.totalCost() << "," << noloResult << ","
            << element.second.getInstructName() << "," << element.second.getInstructEmail() << std::endl;
        
    }

    system("pause");
    return 0;

}
