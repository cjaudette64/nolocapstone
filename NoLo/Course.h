/*
File: Course.h

Course: CSCI285N
Lab Assignment: Capstone NoLo Project

Description: Header file containing the classes Material
and Course. This file stores all of the necessary functions and variable
information for the two classes that are using in MyForm.cpp

Author: Cameron Audette
Date: 04/22/2021

*/

#ifndef COURSE_H
#define COURSE_H

#include <vector>
#include <string>

// Materials class
class Material {

private:
    std::string title;                                  // Stores the title of the book
    std::string author;                                 // Stores the author of the book
    std::string ISBN;                                   // Stores the ISBN of the book
    std::string price;                                  // Stores the cost of the book

public:
    Material(std::vector<std::string>split);            // Constructor that takes a vector
    ~Material();                                        // Destructor

    double getPrice();                                  // Returns the price as a double

    std::string getCanonicalName();                     // Returns the title of the book
                                                        // without general words, symbols,
                                                        // or the authors name
};

// Course class
class Course {

private:
    std::string subject;                                // Stores the subject code for the class
    std::string number;                                 // Stores the course number for the class
    std::string section;                                // Stores the section value for the class
    std::vector<Material> materials;                    // Initializes a vector of the materials
    
    std::string CRN;                                    // Stores the courses CRN (ROOM CSV ONLY)
    std::string name;                                   // Stores the name of the course (ROOM CSV ONLY)
    std::string instructName;                           // Stores the instructors name (ROOM CSV ONLY)
    std::string instructEmail;                          // Stores the instructors email (ROOM CSV ONLY)

public:
    Course();                                           // Constructor
    Course(std::vector<std::string>split);              // Constructor that takes a vector as a parameter
    ~Course();                                          // Destructor

    std::string getID();                                // Returns a string of the full course ID
    std::string getSubject();                           // Returns the subject ID
    std::string getNumber();                            // Returns the course number 
    std::string getSection();                           // Returns the course section
    void addMaterial(Material book);                    // Adds a new material to the material class

    std::string getCRN();                               // Returns the CRN (ROOM CSV ONLY)
    std::string getName();                              // Returns the course name (ROOM CSV ONLY)
    std::string getInstructName();                      // Returns the instructors name (ROOM CSV ONLY)
    std::string getInstructEmail();                     // Returns the instructors email (ROOM CSV ONLY)

    void addRoomInfo(std::vector<std::string>Room);     // Adds Room list info to the current course information

    double totalCost();                                 // Returns the total material cost for a course
};



#endif /* COURSE_H */

