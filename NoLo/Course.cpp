/*
File: Course.cpp

Course: CSCI285N
Lab Assignment: Capstone NoLo Project

Description: Implementation of the Course.h header file. Course.cpp
stores all of the necessary functions and values. This file also defines
each function and creates them accordingly.

Author: Cameron Audette
Date: 04/22/2021

*/

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <iterator>
#include <regex>
#include <map>
#include <string_view>
#include <algorithm>

#include "Course.h"
#include "utils.h"



//****************************************************************
// The Material() constructor sets the values of author,     *
// Title, ISBN, and PRICE for each book in the vector.           *
//****************************************************************
Material::Material(std::vector<std::string>split) {
    author = split[4];
    title = split[5];
    ISBN = split[11];
    price = split[13];
}



//****************************************************************
// The ~Material() member function is an empty destructor.       *
//****************************************************************
Material::~Material() {
}



//****************************************************************
// The getPrice() member function returns the price for each     *
// Material. This function converts it from a string to a double *
// For easier conversions.                                       *
//****************************************************************
double Material::getPrice() {
    std::string tempPrice = price;              // Saves the price into a string
    int symbol = tempPrice.find("$");           // Records the location of the dollar symbol

    // If the dollar symbol was found in the price string
    // Replace it with an empty string
    if (symbol != std::string::npos) {

        tempPrice.replace(symbol, 1, "");
    }
    return atof(tempPrice.c_str());             // Converts the price from a string to a double
}



//****************************************************************
// The getCanonicalName() member function returns the title of a *
// Book after removing common words, the authors name, and       *
// Specific symbols that may be located throughout the title.    *
//****************************************************************
std::string Material::getCanonicalName() {
    std::string name = title;

    name = std::regex_replace(name, std::regex(" THE "), "");
    name = std::regex_replace(name, std::regex(" A "), "");
    name = std::regex_replace(name, std::regex(" OF "), "");
    name = std::regex_replace(name, std::regex(" IS "), "");
    name = std::regex_replace(name, std::regex(" AN "), "");
    name = std::regex_replace(name, std::regex(" AND "), "");
    name = std::regex_replace(name, std::regex(author), "");
    name = std::regex_replace(name, std::regex(":"), "");
    name = std::regex_replace(name, std::regex("\\."), "");
    name = std::regex_replace(name, std::regex(";"), "");
    name = std::regex_replace(name, std::regex(","), "");
    name = std::regex_replace(name, std::regex("\\("), "");
    name = std::regex_replace(name, std::regex("\\)"), "");
    name = std::regex_replace(name, std::regex("'"), "");
    name = std::regex_replace(name, std::regex("/"), "");
    name = std::regex_replace(name, std::regex(" "), "");

    return name;
}



//****************************************************************
// This is an empty constructor for data purposes                *
//****************************************************************
Course::Course() {

}



//****************************************************************
// The Course() constructor takes a string vector that will set  *
// the subject, course number, and the section for each course   *
// and also calculates if the section is an ALL SECTION or other *
//****************************************************************
Course::Course(std::vector<std::string>data) {
    std::vector<std::string>classDiv = split(data[0], ' ');         // Splits the vector of course info
                                                                    // And splits it up.

    subject = classDiv[0];                                          // Sets the subject to the first element
                                                                    // In the vector
    number = classDiv[1];                                           // Sets the subject to the second element
                                                                    // In the vector

    // Convert section to upper case for comparison
    std::string tempSection = classDiv[2];
    transform(tempSection.begin(), tempSection.end(), tempSection.begin(), toupper);

    // If the section contains an "ALL" in it, then the section type
    // is set to "ALL SECTIONS"
    // Else, the section keeps the defualt one provided.
    if (tempSection.find("ALL") != std::string::npos) {
        section = "ALL SECTIONS";
    }
    else {
        section = classDiv[2];
    }
}



//****************************************************************
// The ~Constructor() member function is an empty destructor.       *
//****************************************************************
Course::~Course() {
}



//****************************************************************
// The getID() member function returns a string that contains    *
// The subject, course number, and section for each course       *
//****************************************************************
std::string Course::getID() {
    if (subject != "") {
        return subject + " " + number + " " + section;
    }
    else {
        return "";
    }
}



//****************************************************************
// The getSubject() member function returns the course subject   *
//****************************************************************
std::string Course::getSubject() {
    return subject;
}



//****************************************************************
// The getNumber() member function returns the course number     *
//****************************************************************
std::string Course::getNumber() {
    return number;
}



//****************************************************************
// The getSection() member function returns the section          *
//****************************************************************
std::string Course::getSection() {
    return section;
}



//****************************************************************
// The addMaterial() member function takes the class Material    *
// And adds a new book to the class.                             *
//****************************************************************
void Course::addMaterial(Material book) {
    materials.push_back(book);
}



//****************************************************************
// The getCRN() member function returns the CRN                  *
//****************************************************************
std::string Course::getCRN()
{
    return CRN;
}



//****************************************************************
// The getName() member function returns the course name         *
//****************************************************************
std::string Course::getName()
{
    return name;
}



//****************************************************************
// The getInstructName() member function returns the             *
// Instructors name                                              *
//****************************************************************
std::string Course::getInstructName()
{
    return instructName;
}



//****************************************************************
// The getInstructEmail() member function returns the            *
// Instructors email                                             *
//****************************************************************
std::string Course::getInstructEmail()
{
    return instructEmail;
}



//****************************************************************
// The addRoomInfo() member function takes a vector of the room  *
// list and sets all of the required information appropriately   *
//****************************************************************
void Course::addRoomInfo(std::vector<std::string> Room)
{
    CRN = Room[4];
    section = Room[7];
    name = Room[8];
    instructName = Room[29] + " " + Room[28];
    instructEmail = Room[30];
}



//****************************************************************
// The totalCost() member function returns the total cost for    *
// Section. This takes all of the books that meet the required   *
// Criteria.                                                     *
//****************************************************************
double Course::totalCost() {
    double total = 0;                                                   // Stores the total for each course

    std::map<std::string, double> uniqueBooks;                          // Creates a new map for unique books
    std::string currBookName;                                           // Holds the current book name

    // For each book in material, check to find if the 
    // Current book is shown twice in the materials list
    for (int i = 0; i < materials.size(); i++) {

        currBookName = materials[i].getCanonicalName();                 // Sets the current book to its canonical name

        // If the current book is in the unieue book list, check
        // If the price of the book is greater than the book with the 
        // Same title, and set the book titles price to the higher one
        // Of the two
        if (uniqueBooks.count(currBookName) > 0)
        {
            if (materials[i].getPrice() > uniqueBooks[currBookName])
            {
                uniqueBooks[currBookName] = materials[i].getPrice();
            }
        }

        // Put the current book inside of uniquebooks if it was not found
        // and set it to the price it currently has
        else
        {
            uniqueBooks[currBookName] = materials[i].getPrice();
        }
    }

    // For each string double pair in the uniquebooks map,
    // Calculate the total cost for the course based on the price
    // Provided
    for (std::pair<std::string, double> element : uniqueBooks)
    {
        total += element.second;
    }

    return total;
}
