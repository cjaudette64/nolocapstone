/*
File: NoloForm.cpp

Course: CSCI285N
Lab Assignment: Capstone NoLo Project

Description: Implementation of file read in and output for bookstore and room CSV information.
This file is the main frame of the coding project and processses. The functions within this 
file will evaluate each CSV file for the bookstore and the room csv and capture the required
information necessary.

Author: Cameron Audette
Date: 04/22/2021

*/

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <iterator>
#include <regex>
#include <map>
#include <string_view>

#include "NoLoForm.h"
#include "Course.h"
#include "utils.h"
#include "CommandLine.h"

using namespace NoLo;

[STAThreadAttribute]

/****************************************************************
* main()
* Main will call the GUI to display on the users screen and allow
* Them to interract with it as necessary.
****************************************************************/
int main(cli::array<String^> ^argc) {
    if (argc->Length <= 0) {
        FileImporter mainForm;
        mainForm.ShowDialog();
        return 0;
    }
    else {
        return NoLo::CommandLine(argc);
    }
}

/****************************************************************
* outputFile (vector, string, double, bool)
* This function will take the data provided by the convert function
* And output the data into a csv. This will also run a check to see
* If the room file was included, and if so, excludes any data containing
* The "ALL SECTIONS" attribute
****************************************************************/
void outputFile(std::map<std::string, Course>* courses, std::string output, double noloValue, bool hideAllSections) {

    ofstream outputFile;                // Output stream

    outputFile.open(output);            // create and open the .csv file

    // write the file headers
    outputFile << "CRN,SUBJECT,NUMBER,SECTION,COURSE NAME,COST,NOLO,INSTRUCTOR NAME,INSTRUCTOR EMAIL" << endl;

    // This for loop checks each element within the courses vector map
    // and first looks to see if the room file was give.
    for (std::pair<std::string, Course> element : *courses) {

        // If the room file was given and the course has an "ALL SECTIONS"
        // in the section column, those columns are left out
        if (hideAllSections && element.second.getSection() == "ALL SECTIONS")
        {
            continue;
		}

        // This function calculates the NoLo factor for the output.
        // If the calculated total cost is less than the NoLo
        // Threshold provided, then the course is declared as NoLo
        double savedCost = element.second.totalCost();          // Holds the courses overall cost
        std::string noloResult = "";                            // Contains if the course is NoLo
        if (savedCost <= noloValue) {
            noloResult = "NOLO";
        }

        // This output statement prints out all of the courses information to the csv file
        // under the name "NoloOutput.csv"
        outputFile << element.second.getCRN() <<"," << element.second.getSubject() << "," << element.second.getNumber() << "," << element.second.getSection()
            << "," << element.second.getName() << "," << element.second.totalCost() << "," << noloResult << "," 
            << element.second.getInstructName() << "," << element.second.getInstructEmail() << std::endl;

    }

    outputFile.close();

}


/****************************************************************
* convertButton_Click()
* This function is the main framework for all of the csv analysis.
* This function will start by reading in the book store csv and parse
* Through the csv, grabbing the required infomration. Once this is complete
* The function then grabs the room list if one is available, grabbing the
* Grabbing the required information.
* Once completed, the function will call outputFile to print the data out to
* A CSV file.
****************************************************************/
System::Void NoLo::FileImporter::convertButton_Click(System::Object^ sender, System::EventArgs^ e) {

    // This code will grab the file name of the bookstore csv and store it for
    // File reading
    using namespace Runtime::InteropServices;
    const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(this->bookstoreLabel->Text)).ToPointer();
    std::string str = chars;
    Marshal::FreeHGlobal(IntPtr((void*)chars));


    // This retrieves the NoLo threshold provided by the user
    double noloValue = System::Decimal::ToDouble(this->noloThreshold->Value);


    // These constants are used to pull
    // Specific columns from the bookstore csv
    const int SECTION = 0;
    const int CHECKER = 1;
    const int REQ = 2;
    const int AUTHOR = 4;
    const int TITLE = 5;
    const int DURATION = 10;
    const int ISBN = 11;
    const int PRICE = 13;

    
    std::ifstream infile(str);                          // Reads the bookstore csv file into the program
    std::map<std::string, Course> courses;              // Creates a map and key pair for the courses class
    std::string line;                                   // String to hold each line within the CSV file
    bool fileIntegrity = false;                         // Indicates whether the file is valid or not

    //This if statement ensures that the file is running properly.
    if (infile.good()) {
        Course currCourse;                              // Creates a new course object called currCourse
        
        // While the file can be read in and a line can be retrieved from it,
        // Start the parsing process
        while (getline(infile, line)) {

            // Creates a vector of strings that contains each line of the CSV file.
            // Each line is separated into commas to better read the file
            std::vector<std::string> bookVector = split(line, ',');

            // This file checks to see if the file is valid
            if (bookVector[CHECKER] == "COURSE HISTORY REPORT") {
                fileIntegrity = true;
            }

            // OBTAINING THE COURSE SECTION INFORMATION
            // If the current line being analyzed is not empty and the line doesnt start with
            // the title course, obtain the course information
            if (fileIntegrity) {
                if (bookVector[SECTION].length() != 0 && bookVector[SECTION] != "COURSE") {
                    if (currCourse.getID() != "") {
                        courses[currCourse.getID()] = currCourse;       // Saves the course section inforation
                                                                        // In the new class
                    }
                    currCourse = Course(bookVector);                    // Saves the current course information
                                                                        // In currCourse to be used to get the 
                                                                        // Courses materials
                }


                // OBTAINING A COURSES MATERIALS
                // If the line being read in is a material, this section is performed.
                // If the current course is not empty, we begin to look
                // At the materials for that course
                else {
                    if (currCourse.getID() != "") {

                        // If the material is required and is found in the line, keep going
                        if (bookVector[REQ].find("REQ") != std::string::npos) {

                            // If the line contains PURCHASE or N/A, keep looking into the material
                            if (bookVector[DURATION].find("PURCHASE") != std::string::npos || bookVector[DURATION].find("N/A") != std::string::npos) {

                                // If the material has an ISBN, the material is a book and is added to
                                // The material vector and is assigned to the current course
                                if (bookVector[ISBN] != "") {
                                    Material currMaterial(bookVector);
                                    currCourse.addMaterial(currMaterial);
                                }
                            }
                        }
                    }
                }
            }
        }

        

        // This ensures that all of the course IDs have been saved
        // And placed in the courses class.
        if (currCourse.getID() != "") {
            courses[currCourse.getID()] = currCourse;
        }
    }

    infile.close();

    // This function uses the previous if statement to identify
    // If the file included was valid. If not, the file will
    // Output an error and exit the running process.
    if (fileIntegrity == false) {
        saveFileLabel->Text = "FILE INVALID FOR: BOOKSTORE CSV";
        return;
    }


    // This section of the convertButton_Click() incorporates the room list file
    // If one is provided by the user. This code will retrieve the room file and 
    // Get it ready to be read in to be parsed
    using namespace Runtime::InteropServices;
    const char* charsRoom = (const char*)(Marshal::StringToHGlobalAnsi(this->roomLabel->Text)).ToPointer();
    string strRoom = charsRoom;
    Marshal::FreeHGlobal(IntPtr((void*)charsRoom));


    infile.open(strRoom);               // Opens the file
    std::string lineRoom;               // Initializes a string that will hold each line of the room csv

    bool hasRoomList = false;           // Initializes a boolean to false to be used for the course sections
                                        // Later on
    fileIntegrity = false;
    // If the file read in succeeded, keep going
    if (infile.good()) {

        hasRoomList = true;             // Changes the boolean to true since a room list was included

        // While the file is able to be read in and a line is retrieved from the file
        // Begin processing the data
        while (getline(infile, lineRoom)) {

            // Creates a vector to hold the room csv data
            std::vector<std::string> roomVector = split(lineRoom, ',');

            // Verifies that the file is currently valid
            if (roomVector[AUTHOR] == "CRN" && roomVector[TITLE] == "SUBJ") {
                fileIntegrity = true;
            }

            // If the file is valid and has opened properly, begin
            // Parsing through the room CSV
            if (fileIntegrity) {

                // Check if valid line
                if (roomVector.size() < 31)
                {
                    continue;
                }

                // Creates a string of the ID to be mapped to for each course
                std::string courseID = roomVector[5] + " " + roomVector[6] + " " + roomVector[7];

                // If the course ID created above is in the courses class, add
                // All additional required room list info to the class being reviewed
                // This if will only work if the course does not say all sections
                if (courses.count(courseID) > 0) {

                    courses[courseID].addRoomInfo(roomVector);

                }

                // This else statement will run if the course does state that it is for 
                // All sections. 
                else
                {
                    std::string allSectionsID = roomVector[5] + " " + roomVector[6] + " ALL SECTIONS";

                    // If the course contains all sections, add the proper room infomration to the 
                    // Already included course info.
                    if (courses.count(allSectionsID) > 0) {
                        Course newCourse = courses[allSectionsID];
                        newCourse.addRoomInfo(roomVector);
                        courses[courseID] = newCourse;
                    }
                }
            }
        }
    }

    infile.close();

    // This function uses the previous if statement to identify
    // If the file included was valid and if it was included.
    // If not, the file will output an error and exit the running process.
    if (fileIntegrity == false && hasRoomList == true) {
        saveFileLabel->Text = "FILE INVALID FOR: ROOM CSV";
        return;
    }


    // This code allows the user to select a directory they wish to save the file
    // To and displays the location name on the GUI
    saveFileDialog1->Filter = "CSV (Comma delimited)|*.csv";
    saveFileDialog1->Title = "Save a text file";
    saveFileDialog1->ShowDialog();

    using namespace Runtime::InteropServices;
    const char* saveChars = (const char*)(Marshal::StringToHGlobalAnsi(this->saveFileLabel->Text)).ToPointer();
    string saveFileStr = saveChars;
    Marshal::FreeHGlobal(IntPtr((void*)saveChars));


    // Output all information including the course information, file name (can be changed),
    // NoLo threshold, and bool for if the room list was included to the outputFile function
    outputFile(&courses, saveFileStr, noloValue, hasRoomList);

}

