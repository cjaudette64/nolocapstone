#pragma once

/*
File: NoLoForm.h

Course: CSCI285N
Lab Assignment: Capstone NoLo Project

Description: Class implementation file and GUI organization file for the entire project.
This file contains all of the specifications for the graphical user interface and identifies
All of the variables and values necessary to receive user input

Author: Cameron Audette
Date: 04/22/2021

*/

#include <iostream>
#include <fstream>
#include <string>


using namespace std;

namespace NoLo {

    using namespace System;
    using namespace System::ComponentModel;
    using namespace System::Collections;
    using namespace System::Windows::Forms;
    using namespace System::Data;
    using namespace System::Drawing;

    /// <summary>
    /// Summary for MyForm
    /// </summary>
    public ref class FileImporter : public System::Windows::Forms::Form
    {
    public:
        FileImporter(void)
        {
            InitializeComponent();
            //
            //TODO: Add the constructor code here
            //
        }


    protected:
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        ~FileImporter()
        {
            if (components)
            {
                delete components;
            }
        }
    private: System::Windows::Forms::Button^ bookstoreButton;
    private: System::Windows::Forms::OpenFileDialog^ openFileDialogBookstore;
    private: System::Windows::Forms::Label^ bookstoreLabel;
    private: System::Windows::Forms::Button^ roomButton;
    private: System::Windows::Forms::OpenFileDialog^ openFileDialogRoom;
    private: System::Windows::Forms::Label^ roomLabel;
    private: System::Windows::Forms::Label^ label1;
    private: System::Windows::Forms::Button^ calculateButton;

    private: System::Windows::Forms::Button^ doneButton;
    private: System::Windows::Forms::Button^ resetButton;
    private: System::Windows::Forms::NumericUpDown^ noloThreshold;

    private: System::Windows::Forms::Label^ label2;
    private: System::Windows::Forms::Button^ button1;



    private: System::Windows::Forms::SaveFileDialog^ saveFileDialog1;
    private: System::Windows::Forms::Label^ saveFileLabel;







    protected:

    protected:


    private:
        /// <summary>
        /// Required designer variable.
        /// </summary>
        System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
            this->bookstoreButton = (gcnew System::Windows::Forms::Button());
            this->openFileDialogBookstore = (gcnew System::Windows::Forms::OpenFileDialog());
            this->bookstoreLabel = (gcnew System::Windows::Forms::Label());
            this->roomButton = (gcnew System::Windows::Forms::Button());
            this->openFileDialogRoom = (gcnew System::Windows::Forms::OpenFileDialog());
            this->roomLabel = (gcnew System::Windows::Forms::Label());
            this->label1 = (gcnew System::Windows::Forms::Label());
            this->calculateButton = (gcnew System::Windows::Forms::Button());
            this->doneButton = (gcnew System::Windows::Forms::Button());
            this->resetButton = (gcnew System::Windows::Forms::Button());
            this->noloThreshold = (gcnew System::Windows::Forms::NumericUpDown());
            this->label2 = (gcnew System::Windows::Forms::Label());
            this->button1 = (gcnew System::Windows::Forms::Button());
            this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
            this->saveFileLabel = (gcnew System::Windows::Forms::Label());
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->noloThreshold))->BeginInit();
            this->SuspendLayout();
            // 
            // bookstoreButton
            // 
            this->bookstoreButton->AccessibleName = L"bookstoreButton";
            this->bookstoreButton->Location = System::Drawing::Point(12, 58);
            this->bookstoreButton->Name = L"bookstoreButton";
            this->bookstoreButton->Size = System::Drawing::Size(198, 23);
            this->bookstoreButton->TabIndex = 0;
            this->bookstoreButton->Text = L"Bookstore CSV File";
            this->bookstoreButton->UseVisualStyleBackColor = true;
            this->bookstoreButton->Click += gcnew System::EventHandler(this, &FileImporter::btnBookstore_Click);
            // 
            // openFileDialogBookstore
            // 
            this->openFileDialogBookstore->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &FileImporter::openFileDialogBookstore_FileOk);
            // 
            // bookstoreLabel
            // 
            this->bookstoreLabel->AccessibleName = L"";
            this->bookstoreLabel->AutoSize = true;
            this->bookstoreLabel->Location = System::Drawing::Point(260, 58);
            this->bookstoreLabel->Name = L"bookstoreLabel";
            this->bookstoreLabel->Size = System::Drawing::Size(0, 13);
            this->bookstoreLabel->TabIndex = 1;
            // 
            // roomButton
            // 
            this->roomButton->AccessibleName = L"roomButton";
            this->roomButton->Location = System::Drawing::Point(12, 121);
            this->roomButton->Name = L"roomButton";
            this->roomButton->Size = System::Drawing::Size(198, 23);
            this->roomButton->TabIndex = 2;
            this->roomButton->Text = L"Room CSV File";
            this->roomButton->UseVisualStyleBackColor = true;
            this->roomButton->Click += gcnew System::EventHandler(this, &FileImporter::btnRoom_Click);
            // 
            // openFileDialogRoom
            // 
            this->openFileDialogRoom->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &FileImporter::openFileDialogRoom_FileOk);
            // 
            // roomLabel
            // 
            this->roomLabel->AccessibleName = L"";
            this->roomLabel->AutoSize = true;
            this->roomLabel->Location = System::Drawing::Point(260, 121);
            this->roomLabel->Name = L"roomLabel";
            this->roomLabel->Size = System::Drawing::Size(0, 13);
            this->roomLabel->TabIndex = 1;
            this->roomLabel->Click += gcnew System::EventHandler(this, &FileImporter::roomLabel_Click);
            // 
            // label1
            // 
            this->label1->AutoSize = true;
            this->label1->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
            this->label1->Location = System::Drawing::Point(12, 18);
            this->label1->Name = L"label1";
            this->label1->RightToLeft = System::Windows::Forms::RightToLeft::No;
            this->label1->Size = System::Drawing::Size(391, 13);
            this->label1->TabIndex = 3;
            this->label1->Text = L"NoLo File Converter: Please select the proper CSV files below, then click convert"
                L".";
            this->label1->Click += gcnew System::EventHandler(this, &FileImporter::label1_Click);
            // 
            // calculateButton
            // 
            this->calculateButton->Location = System::Drawing::Point(12, 244);
            this->calculateButton->Name = L"calculateButton";
            this->calculateButton->Size = System::Drawing::Size(128, 23);
            this->calculateButton->TabIndex = 4;
            this->calculateButton->Text = L"Calculate";
            this->calculateButton->UseVisualStyleBackColor = true;
            this->calculateButton->Click += gcnew System::EventHandler(this, &FileImporter::convertButton_Click);
            // 
            // doneButton
            // 
            this->doneButton->Location = System::Drawing::Point(12, 361);
            this->doneButton->Name = L"doneButton";
            this->doneButton->Size = System::Drawing::Size(128, 23);
            this->doneButton->TabIndex = 5;
            this->doneButton->Text = L"Exit";
            this->doneButton->UseVisualStyleBackColor = true;
            this->doneButton->Click += gcnew System::EventHandler(this, &FileImporter::exit_Click);
            // 
            // resetButton
            // 
            this->resetButton->Location = System::Drawing::Point(156, 361);
            this->resetButton->Name = L"resetButton";
            this->resetButton->Size = System::Drawing::Size(128, 23);
            this->resetButton->TabIndex = 6;
            this->resetButton->Text = L"Reset";
            this->resetButton->UseVisualStyleBackColor = true;
            this->resetButton->Click += gcnew System::EventHandler(this, &FileImporter::reset_click);
            // 
            // noloThreshold
            // 
            this->noloThreshold->Location = System::Drawing::Point(12, 189);
            this->noloThreshold->Name = L"noloThreshold";
            this->noloThreshold->Size = System::Drawing::Size(120, 20);
            this->noloThreshold->TabIndex = 7;
            this->noloThreshold->ValueChanged += gcnew System::EventHandler(this, &FileImporter::numericUpDown1_ValueChanged);
            // 
            // label2
            // 
            this->label2->AutoSize = true;
            this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
                static_cast<System::Byte>(0)));
            this->label2->Location = System::Drawing::Point(138, 189);
            this->label2->Name = L"label2";
            this->label2->Size = System::Drawing::Size(129, 15);
            this->label2->TabIndex = 8;
            this->label2->Text = L"NoLo Threshold Value";
            // 
            // button1
            // 
            this->button1->Location = System::Drawing::Point(605, 18);
            this->button1->Name = L"button1";
            this->button1->Size = System::Drawing::Size(75, 23);
            this->button1->TabIndex = 9;
            this->button1->Text = L"Help";
            this->button1->UseVisualStyleBackColor = true;
            this->button1->Click += gcnew System::EventHandler(this, &FileImporter::help_Click);
            // 
            // saveFileDialog1
            // 
            this->saveFileDialog1->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &FileImporter::openFileDialogSave_FileOk);
            // 
            // saveFileLabel
            // 
            this->saveFileLabel->AutoSize = true;
            this->saveFileLabel->Location = System::Drawing::Point(175, 244);
            this->saveFileLabel->Name = L"saveFileLabel";
            this->saveFileLabel->Size = System::Drawing::Size(0, 13);
            this->saveFileLabel->TabIndex = 10;
            // 
            // FileImporter
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(692, 392);
            this->Controls->Add(this->saveFileLabel);
            this->Controls->Add(this->button1);
            this->Controls->Add(this->label2);
            this->Controls->Add(this->noloThreshold);
            this->Controls->Add(this->resetButton);
            this->Controls->Add(this->doneButton);
            this->Controls->Add(this->calculateButton);
            this->Controls->Add(this->label1);
            this->Controls->Add(this->roomLabel);
            this->Controls->Add(this->roomButton);
            this->Controls->Add(this->bookstoreLabel);
            this->Controls->Add(this->bookstoreButton);
            this->Name = L"FileImporter";
            this->Text = L"NoLo Wizard";
            this->Load += gcnew System::EventHandler(this, &FileImporter::FileImporter_Load);
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->noloThreshold))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
        //Bookstore button and label set up
    private: System::Void openFileDialogBookstore_FileOk(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e) {

        // Thanks to: https://docs.microsoft.com/en-us/cpp/dotnet/how-to-convert-system-string-to-standard-string?view=msvc-160
        using namespace Runtime::InteropServices;
        const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(this->openFileDialogBookstore->FileName)).ToPointer();
        string str = chars;
        Marshal::FreeHGlobal(IntPtr((void*)chars));
        cout << str << endl;

        this->bookstoreLabel->Text = this->openFileDialogBookstore->FileName;
    }

    private: System::Void btnBookstore_Click(System::Object^ sender, System::EventArgs^ e) {
        this->openFileDialogBookstore->ShowDialog();

    }
           //End of bookstore and label set up

    private: System::Void openFileDialogRoom_FileOk(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e) {

        // Thanks to: https://docs.microsoft.com/en-us/cpp/dotnet/how-to-convert-system-string-to-standard-string?view=msvc-160
        using namespace Runtime::InteropServices;
        const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(this->openFileDialogRoom->FileName)).ToPointer();
        string str = chars;
        Marshal::FreeHGlobal(IntPtr((void*)chars));
        cout << str << endl;

        this->roomLabel->Text = this->openFileDialogRoom->FileName;
    }

    private: System::Void btnRoom_Click(System::Object^ sender, System::EventArgs^ e) {
        this->openFileDialogRoom->ShowDialog();

    }

    private: System::Void FileImporter_Load(System::Object^ sender, System::EventArgs^ e) {
    }
    private: System::Void label1_Click(System::Object^ sender, System::EventArgs^ e) {
    }

    private: System::Void convertButton_Click(System::Object^ sender, System::EventArgs^ e);

    private: System::Void roomLabel_Click(System::Object^ sender, System::EventArgs^ e) {
    }
    private: System::Void numericUpDown1_ValueChanged(System::Object^ sender, System::EventArgs^ e) {

    }


    private: System::Void exit_Click(System::Object^ sender, System::EventArgs^ e) {
        Application::Exit();
    }

    private: System::Void help_Click(System::Object^ sender, System::EventArgs^ e) {
        System::Windows::Forms::MessageBox::Show("COMMONLY ASKED QUESTIONS" +
            "\n\n1. I continue to get an error message regarding a file issue / I keep getting" +
            " missing columns while calculating with both CSVs. How do I fix this? \nANSWER: Start out by verifying that both the bookstore and room files are" +
            " CSV files. You can do this by exporting the files from excel as a .csv\n\n" +
            "2. I keep receiving the file output that includes the room information and the bookstore information, but the NoLo threshold is not correct." +
            " How can I change this? \nANSWER: Start by ensuring that the file bookstore file includes the cost of each book in the 14th column. Then" +
            " verifty that the NoLo Threshold numeric changer is set to a value other than 0. You may also attempt to use the reset button to" +
            " fully ensure the field was cleared.\n\n" +
            "3. What files can I use to run this program? \nANSWER: You can use CSV files that are set up for the bookstore and room list of courses." +
            " Please see the proper formatting of the sample output files provided.\n\n" +
            "4. Who can I contact if I have issues running this program or understanding how to add new columns? \nANSWER: Feel free to reach out " +
            "to the author of this program, Cameron Audette, via email: caudette192@students.ccsnh.edu for further inquiries or questions", "Help Feature");
    }

    private: System::Void reset_click(System::Object^ sender, System::EventArgs^ e) {
        bookstoreLabel->Text = "";
        roomLabel->Text = "";
        noloThreshold->Value = 0;
        saveFileLabel->Text = "";
    }
    
    private: System::Void storeName(System::Object^ sender, System::EventArgs^ e) {
    }
    private: System::Void openFileDialogSave_FileOk(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e) {

        this->saveFileLabel->Text = this->saveFileDialog1->FileName;
    }
    };
}