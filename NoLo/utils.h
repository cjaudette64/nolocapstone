/*
File: utils.h

Course: CSCI285N
Lab Assignment: Capstone NoLo Project

Description: Header file for utils.cpp that contains the
definition of the split vector function

Author: Cameron Audette
Date: 04/22/2021

*/

#pragma once

#include <regex>
#include <string>
#include <string_view>
#include <vector>

std::vector<std::string> split(const std::string& s, char delim);