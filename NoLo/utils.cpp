/*
File: utils.cpp

Course: CSCI285N
Lab Assignment: Capstone NoLo Project

Description: Implementation file for utils.h. This file implements
the functions specified in utils.h.

Author: Cameron Audette
Date: 04/22/2021

*/

#include <regex>
#include <string>
#include <string_view>
#include <vector>
#include <sstream>

#include "utils.h"



//****************************************************************
// The trim() member function returns a string that is provided  *
// By trimming down the leading and trailing spaces              *
//****************************************************************
std::string trim(std::string s) {
    std::regex e("^\\s+|\\s+$"); // remove leading and trailing spaces
    return std::regex_replace(s, e, "");
}



//****************************************************************
// The split() member function takes a string, a character, and  *
// Out template result and splits each line of the data into     *
// A comma separated trimmed string                              *
//****************************************************************
template <typename Out>          //Creates a new template with the name Out
void split(const std::string& s, char delim, Out result) {
    std::istringstream iss(s);
    std::string item;
    std::string inner;
    while (std::getline(iss, item, delim)) {
        if (item[0] == '"') {
            int l = item.length();
            if (item[l - 1] != '"') {
                item.append(",");
                item.erase(0, 1);
                std::getline(iss, inner, '"');
                item.append(inner);
                std::getline(iss, inner, delim);
            }
        }
        *result++ = trim(item);
    }
}



//****************************************************************
// The split() member function returns a vector and takes a      *
// String and char and returns the split elements vector elems   *
//****************************************************************
std::vector<std::string> split(const std::string& s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}
